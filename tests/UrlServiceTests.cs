﻿using AutoFixture;
using hey_url_challenge_code_dotnet.Exceptions;
using hey_url_challenge_code_dotnet.Models;
using hey_url_challenge_code_dotnet.Service;
using HeyUrlChallengeCodeDotnet.Data;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace tests
{
    [TestFixture]
    public class UrlServiceTests
    {
        private ApplicationContext applicationContext;
        private IUrlService urlService;
        private Fixture fixture;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: "HeyUrlTests")
                .Options;

            this.applicationContext = new ApplicationContext(options);   
            this.urlService = new UrlService(this.applicationContext);
            this.fixture = new Fixture();

            this.applicationContext.Database.EnsureCreated();
        }

        [TearDown]
        public void TearDown()
        {
            this.applicationContext.Database.EnsureDeleted();
            this.applicationContext.Dispose();
        }

        [Test]
        public void WhenShortingUrl_WithEmptyUrl_ShouldThrowsUrlShortnerException()
        {
            var exception = Assert.ThrowsAsync<UrlShortnerException>(async () => await this.urlService.ShortenUrlAsync(string.Empty));

            Assert.IsNotNull(exception);
            Assert.AreEqual("You provided an empty text to short. Please type a valid url.", exception.Message);
        }

        [Test]
        public void WhenShortingUrl_WithInvalidUrl_ShouldThrowsUrlShortnerException()
        {
            var url = "abcefgh";
            var exception = Assert.ThrowsAsync<UrlShortnerException>(async () => await this.urlService.ShortenUrlAsync(url));

            Assert.IsNotNull(exception);
            Assert.AreEqual("You provided an invalid url to short. Please type a valid url.", exception.Message);
        }

        [Test]
        public void WhenShortingUrl_WithInaccessibleUrl_ShouldThrowsUrlShortnerException()
        {
            var url = "http://abcefgh.com";
            var exception = Assert.ThrowsAsync<UrlShortnerException>(async () => await this.urlService.ShortenUrlAsync(url));

            Assert.IsNotNull(exception);
            Assert.AreEqual("You provided an invalid url to short. Please type a valid url.", exception.Message);
        }

        [Test]
        public void WhenShortingUrl_WithAlreadShortedUrl_ShouldThrowsUrlShortnerException()
        {
            var url = "https://www.fullstacklabs.co";

            var model = new Url
            {
                OriginalUrl = url,
                ShortUrl = "ABCDE"
            };

            this.applicationContext.Urls.Add(model);
            this.applicationContext.SaveChanges();
            
            var exception = Assert.ThrowsAsync<UrlShortnerException>(async () => await this.urlService.ShortenUrlAsync(url));

            Assert.IsNotNull(exception);
            Assert.AreEqual("This url already have been shorted and it is available in the list below.", exception.Message);
        }

        [Test]
        public async Task WhenShortingUrl_WithValidUrl_ShouldCreateSuccessfully()
        {
            var url = "https://www.fullstacklabs.co";

            var shortUrl = await this.urlService.ShortenUrlAsync(url);

            var createdUrl = await this.urlService.GetByShortUrl(shortUrl);

            Assert.IsNotNull(shortUrl);
            Assert.AreEqual(5, shortUrl.Length);
            Assert.IsTrue(Regex.IsMatch(shortUrl, "[A-Z]"));
            Assert.AreEqual(shortUrl.ToUpper(), shortUrl);
            Assert.IsNotNull(createdUrl);
        }

        [Test]
        public void WhenSavingUrlClick_WithNotFoundUrl_ShouldThrowsUrlNotFoundException()
        {
            var url = fixture.Create<string>().Substring(0, 5).ToUpper();
            var browser = fixture.Create<string>();
            var platform = fixture.Create<string>();

            var exception = Assert.ThrowsAsync<ShortUrlNotFoundException>(async () => 
                await this.urlService.SaveClickMetrics(url, browser, platform));

            Assert.IsNotNull(exception);
            Assert.AreEqual($"Provided URL was not found ({url}).", exception.Message);
        }

        [Test]
        public async Task WhenSavingUrlClick_WithValidUrlAndFirstClickAtDay_ShouldSaveClickPerDayBrowserAndPlatform()
        {
            var shortUrl = fixture.Create<string>().Substring(0, 5).ToUpper();
            var browser = fixture.Create<string>();
            var platform = fixture.Create<string>();

            var url = fixture.Build<Url>()
                .With(x => x.ShortUrl, shortUrl)
                .Without(x => x.ClickMetrics)
                .Without(x => x.BrowserMetrics)
                .Without(x => x.PlatformMetrics)
                .Create();

            this.applicationContext.Urls.Add(url);
            this.applicationContext.SaveChanges();

            await this.urlService.SaveClickMetrics(shortUrl, browser, platform);

            var clicksMetric = this.applicationContext.UrlClickMetrics.FirstOrDefault(x => x.Url.ShortUrl == shortUrl);
            var browserMetric = this.applicationContext.UrlBrowserMetrics.FirstOrDefault(x => x.Url.ShortUrl == shortUrl);
            var platformMetric = this.applicationContext.UrlPlatformMetrics.FirstOrDefault(x => x.Url.ShortUrl == shortUrl);

            Assert.IsNotNull(clicksMetric);
            Assert.AreEqual(1, clicksMetric.Count);
            Assert.AreEqual(DateTime.UtcNow.Date, clicksMetric.Date);

            Assert.IsNotNull(browserMetric);
            Assert.AreEqual(1, browserMetric.Count);
            Assert.AreEqual(DateTime.UtcNow.Date, browserMetric.Date);

            Assert.IsNotNull(platformMetric);
            Assert.AreEqual(1, platformMetric.Count);
            Assert.AreEqual(DateTime.UtcNow.Date, platformMetric.Date);
        }

        [Test]
        public async Task WhenSavingUrlClick_WithValidUrlAndExistingMetricAtDay_ShouldIncreaseClickPerDayBrowserAndPlatform()
        {
            var shortUrl = fixture.Create<string>().Substring(0, 5).ToUpper();
            var browser = fixture.Create<string>();
            var platform = fixture.Create<string>();

            var clicksCount = fixture.Create<int>();
            var browsersCount = fixture.Create<int>();
            var platformsCount = fixture.Create<int>();

            var url = fixture.Build<Url>()
                .With(x => x.ShortUrl, shortUrl)
                .Without(x => x.ClickMetrics)
                .Without(x => x.BrowserMetrics)
                .Without(x => x.PlatformMetrics)
                .Create();

            var clickMetric = fixture.Build<UrlClickMetrics>()
                .With(x => x.Url, url)
                .With(x => x.Count, clicksCount)
                .Without(x => x.Date)
                .Create();

            var browserMetric = fixture.Build<UrlBrowserMetrics>()
                .With(x => x.Url, url)
                .With(x => x.Browser, browser)
                .With(x => x.Count, browsersCount)
                .Without(x => x.Date)
                .Create();

            var platformMetric = fixture.Build<UrlPlatformMetrics>()
                .With(x => x.Url, url)
                .With(x => x.Platform, platform)
                .With(x => x.Count, platformsCount)
                .Without(x => x.Date)
                .Create();

            this.applicationContext.Urls.Add(url);
            this.applicationContext.UrlClickMetrics.Add(clickMetric);
            this.applicationContext.UrlBrowserMetrics.Add(browserMetric);
            this.applicationContext.UrlPlatformMetrics.Add(platformMetric);

            this.applicationContext.SaveChanges();

            await this.urlService.SaveClickMetrics(shortUrl, browser, platform);

            var persistedClicksMetric = this.applicationContext.UrlClickMetrics
                .FirstOrDefault(x => x.Url.ShortUrl == shortUrl);

            var persistedBrowserMetric = this.applicationContext.UrlBrowserMetrics
                .FirstOrDefault(x => x.Url.ShortUrl == shortUrl && x.Browser == browser);

            var persistedPlatformMetric = this.applicationContext.UrlPlatformMetrics
                .FirstOrDefault(x => x.Url.ShortUrl == shortUrl && x.Platform == platform);

            Assert.IsNotNull(persistedClicksMetric);
            Assert.AreEqual(clicksCount + 1, persistedClicksMetric.Count);
            Assert.AreEqual(clickMetric.Date, persistedClicksMetric.Date);

            Assert.IsNotNull(persistedBrowserMetric);
            Assert.AreEqual(browsersCount + 1, persistedBrowserMetric.Count);
            Assert.AreEqual(browserMetric.Date, persistedBrowserMetric.Date);

            Assert.IsNotNull(platformMetric);
            Assert.AreEqual(platformsCount + 1, persistedPlatformMetric.Count);
            Assert.AreEqual(platformMetric.Date, persistedPlatformMetric.Date);
        }
    }
}