﻿using hey_url_challenge_code_dotnet.Exceptions;
using hey_url_challenge_code_dotnet.Models;
using HeyUrlChallengeCodeDotnet.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace hey_url_challenge_code_dotnet.Service
{
    public class UrlService : IUrlService
    {
        public UrlService(ApplicationContext applicationContext)
        {
            this.applicationContext = applicationContext;
            
            this.beginOfTheDay = DateTime.UtcNow.Date;
            this.endOfTheDay = beginOfTheDay.AddHours(23).AddMinutes(59).AddSeconds(59);
            this.beginOfTheMonth = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1, 0, 0, 0);
            this.endOfTheMonth = beginOfTheMonth.AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        private readonly ApplicationContext applicationContext;
        private readonly DateTime beginOfTheDay;
        private readonly DateTime endOfTheDay;
        private readonly DateTime beginOfTheMonth;
        private readonly DateTime endOfTheMonth;

        #region Read

        public void Teste()
        {
            var a = (from u in this.applicationContext.Urls
                     from c in u.ClickMetrics.Where(uc => uc.Url.Id == u.Id).DefaultIfEmpty()
                     group c by u into r
                     select new
                     {
                         Id = r.Key.Id,
                         Count = r.Sum(x => x.Count)
                     })
                    .ToList();
        }

        public async Task<Url> GetByShortUrl(string shortUrl)
        {
            return await this.applicationContext.Urls.FirstOrDefaultAsync(u => u.ShortUrl == shortUrl);
        }

        public Task<ICollection<Url>> GetAll()
        {
            return this.GetAll(-1, -1);
        }

        public async Task<ICollection<Url>> GetAll(int page, int size)
        {
            var set = this.applicationContext.Urls;
            
            if (page > -1)
            {
                set.Skip(page * size).Take(size);
            }
            
            return await set.Include(u => u.ClickMetrics).ToListAsync();
        }

        public async Task<IDictionary<int, int>> CountClicksPerDayInCurrentMontByShortUrl(string shortUrl)
        {
            var totalDays = DateTime.DaysInMonth(DateTime.UtcNow.Year, DateTime.UtcNow.Month);
            var days = Enumerable.Range(1, totalDays).ToList();

            var clicks = await this.applicationContext.UrlClickMetrics
                .Where(x => x.Url.ShortUrl == shortUrl
                    && x.Date >= this.beginOfTheMonth
                    && x.Date <= this.endOfTheMonth)
                .ToDictionaryAsync(x => x.Date.Day, x => x.Count);

            days.ForEach(d =>
            {
                if (!clicks.ContainsKey(d))
                {
                    clicks.TryAdd(d, 0);
                }
            });

            return clicks;
        }

        public async Task<IDictionary<string, int>> CountClicksPerBrowserInCurrentMontByShortUrl(string shortUrl)
        {
            var clicks = await this.applicationContext.UrlBrowserMetrics
                .Where(x => x.Url.ShortUrl == shortUrl
                    && x.Date >= this.beginOfTheMonth
                    && x.Date <= this.endOfTheMonth)
                .ToDictionaryAsync(x => x.Browser, x => x.Count);

            return clicks;
        }

        public async Task<IDictionary<string, int>> CountClicksPerPlatformInCurrentMontByShortUrl(string shortUrl)
        {
            var clicks = await this.applicationContext.UrlPlatformMetrics
                .Where(x => x.Url.ShortUrl == shortUrl
                    && x.Date >= this.beginOfTheMonth
                    && x.Date <= this.endOfTheMonth)
                .ToDictionaryAsync(x => x.Platform, x => x.Count);

            return clicks;
        }

        #endregion

        #region Write

        public async Task<string> ShortenUrlAsync(string originalUrl)
        {
            if (string.IsNullOrEmpty(originalUrl))
            {
                throw new UrlShortnerException("You provided an empty text to short. Please type a valid url.");
            }

            try
            {
                using (var client = new HttpClient())
                {
                    var formatedUrl = originalUrl.StartsWith("http://") || originalUrl.StartsWith("https://")
                        ? originalUrl
                        : $"http://{originalUrl}";

                    var uri = new Uri(formatedUrl);

                    client.BaseAddress = uri;

                    var result = await client.GetAsync(uri);

                    if (!result.IsSuccessStatusCode)
                        throw new HttpRequestException();
                }
            }
            catch
            {
                throw new UrlShortnerException("You provided an invalid url to short. Please type a valid url.");
            }

            var withoutProtocolUrl = originalUrl.Replace("https://", string.Empty).Replace("http://", string.Empty);
            var httpUrl = $"http://{withoutProtocolUrl}";
            var httpsUrl = $"https://{withoutProtocolUrl}";

            var url = await this.applicationContext.Urls.FirstOrDefaultAsync(x => x.OriginalUrl == withoutProtocolUrl
                || x.OriginalUrl == httpUrl
                || x.OriginalUrl == httpsUrl);

            if (url != null)
            {
                throw new UrlShortnerException("This url already have been shorted and it is available in the list below.");
            }

            url = new Url
            {
                OriginalUrl = originalUrl,
                ShortUrl = this.GenerateShortString()
            };

            this.applicationContext.Add<Url>(url);
            await this.applicationContext.SaveChangesAsync();

            return url.ShortUrl;
        }

        public async Task SaveClickMetrics(string url, string browser, string platform)
        {
            browser = string.IsNullOrEmpty(browser) || string.IsNullOrWhiteSpace(browser) ? "Other" : browser;
            platform = string.IsNullOrEmpty(platform) || string.IsNullOrWhiteSpace(platform) ? "Other" : platform;

            var model = await this.applicationContext.Urls.FirstOrDefaultAsync(u => u.ShortUrl == url);

            if (model == null)
                throw new ShortUrlNotFoundException($"Provided URL was not found ({url}).");

            await this.AddClickMetric(model);
            await this.AddBrowserMetric(model, browser);
            await this.AddPlatformMetric(model, platform);

            model.Count++;
            this.applicationContext.SaveChanges();
        }

        #endregion

        #region Private

        private string GenerateShortString()
        {
            var rand = new Random();
            var builder = new StringBuilder();
            var generateOther = true;

            while (generateOther)
            {
                for (int i = 0; i < 5; i++)
                {
                    builder.Append((char)rand.Next(65, 91));
                }

                generateOther = this.applicationContext.Urls.FirstOrDefault(x => x.ShortUrl == builder.ToString()) != null;
            }

            return builder.ToString();
        }

        private async Task AddClickMetric(Url model)
        {
            var metric = await this.applicationContext.UrlClickMetrics
                .FirstOrDefaultAsync(m => m.Url.Id == model.Id 
                    && m.Date >= this.beginOfTheDay
                    && m.Date <= this.endOfTheDay)
                ?? new UrlClickMetrics(model);

            metric.Count++;

            if (this.applicationContext.Entry<UrlClickMetrics>(metric).State == EntityState.Detached)
            {
                this.applicationContext.Add<UrlClickMetrics>(metric);
            }

            await this.applicationContext.SaveChangesAsync();
        }

        private async Task AddBrowserMetric(Url model, string browser)
        {
            var metric = await this.applicationContext.UrlBrowserMetrics
                .FirstOrDefaultAsync(m => m.Url.Id == model.Id
                    && m.Date >= this.beginOfTheDay
                    && m.Date <= this.endOfTheDay
                    && m.Browser == browser)
                ?? new UrlBrowserMetrics(model, browser);

            metric.Count++;

            if (this.applicationContext.Entry<UrlBrowserMetrics>(metric).State == EntityState.Detached)
            {
                this.applicationContext.Add<UrlBrowserMetrics>(metric);
            }

            await this.applicationContext.SaveChangesAsync();
        }

        private async Task AddPlatformMetric(Url model, string platform)
        {
            var metric = await this.applicationContext.UrlPlatformMetrics
                .FirstOrDefaultAsync(m => m.Url.Id == model.Id
                    && m.Date >= this.beginOfTheDay
                    && m.Date <= this.endOfTheDay
                    && m.Platform == platform)
                ?? new UrlPlatformMetrics(model, platform);

            metric.Count++;

            if (this.applicationContext.Entry<UrlPlatformMetrics>(metric).State == EntityState.Detached)
            {
                this.applicationContext.Add<UrlPlatformMetrics>(metric);
            }

            await this.applicationContext.SaveChangesAsync();
        }

        #endregion
    }
}
