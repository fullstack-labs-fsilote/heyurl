﻿using hey_url_challenge_code_dotnet.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace hey_url_challenge_code_dotnet.Service
{
    public interface IUrlService
    {
        Task<Url> GetByShortUrl(string shortUrl);
        Task<ICollection<Url>> GetAll();
        Task<ICollection<Url>> GetAll(int page, int size);
        Task<IDictionary<int, int>> CountClicksPerDayInCurrentMontByShortUrl(string shortUrl);
        Task<IDictionary<string, int>> CountClicksPerBrowserInCurrentMontByShortUrl(string shortUrl);
        Task<IDictionary<string, int>> CountClicksPerPlatformInCurrentMontByShortUrl(string shortUrl);

        Task<string> ShortenUrlAsync(string url);
        Task SaveClickMetrics(string url, string browser, string platform);
    }
}
