using hey_url_challenge_code_dotnet.Models;
using Microsoft.EntityFrameworkCore;

namespace HeyUrlChallengeCodeDotnet.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        public DbSet<Url> Urls { get; set; }
        public DbSet<UrlClickMetrics> UrlClickMetrics { get; set; }
        public DbSet<UrlPlatformMetrics> UrlPlatformMetrics { get; set; }
        public DbSet<UrlBrowserMetrics> UrlBrowserMetrics { get; set; }
    }
}