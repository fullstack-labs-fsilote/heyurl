﻿using System;

namespace hey_url_challenge_code_dotnet.Exceptions
{
    public class UrlShortnerException : Exception
    {
        public UrlShortnerException() { }

        public UrlShortnerException (string message) : base(message) { }
    }
}
