﻿using System;

namespace hey_url_challenge_code_dotnet.Exceptions
{
    public class ShortUrlNotFoundException : Exception
    {
        public ShortUrlNotFoundException() { }

        public ShortUrlNotFoundException(string message) : base(message) { }
    }
}
