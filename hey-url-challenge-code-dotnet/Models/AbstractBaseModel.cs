﻿using System;

namespace hey_url_challenge_code_dotnet.Models
{
    public class AbstractBaseModel
    {
        public Guid Id { get; set; }
    }
}
