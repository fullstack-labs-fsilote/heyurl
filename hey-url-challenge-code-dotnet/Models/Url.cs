﻿using System;
using System.Collections.Generic;

namespace hey_url_challenge_code_dotnet.Models
{
    public class Url : AbstractBaseModel
    {
        public string OriginalUrl { get; set; }
        public string ShortUrl { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public int Count { get; set; }

        public IList<UrlClickMetrics> ClickMetrics { get; set; } = new List<UrlClickMetrics>();
        public IList<UrlPlatformMetrics> PlatformMetrics { get; set; } = new List<UrlPlatformMetrics>();
        public IList<UrlBrowserMetrics> BrowserMetrics { get; set; } = new List<UrlBrowserMetrics>();
    }
}
