﻿using System;

namespace hey_url_challenge_code_dotnet.Models
{
    public class UrlBrowserMetrics : AbstractBaseModel
    {
        public UrlBrowserMetrics() { }

        public UrlBrowserMetrics(Url url, string browser)
        {
            this.Url = url;
            this.UrlId = url.Id;
            this.Browser = browser;
        }

        public string Browser { get; set; }
        public DateTime Date { get; set; } = DateTime.UtcNow.Date;
        public int Count { get; set; } = 0;

        public Guid UrlId { get; set; }
        public Url Url { get; set; }
    }
}
