﻿using System;

namespace hey_url_challenge_code_dotnet.Models
{
    public class UrlClickMetrics : AbstractBaseModel
    {
        public UrlClickMetrics() { }

        public UrlClickMetrics(Url url)
        {
            this.Url = url;
            this.UrlId = url.Id; 
        }

        public DateTime Date { get; set; } = DateTime.UtcNow.Date;
        public int Count { get; set; }

        public Guid UrlId { get; set; }
        public Url Url { get; set; }
    }
}
