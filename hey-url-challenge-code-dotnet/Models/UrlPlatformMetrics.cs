﻿using System;

namespace hey_url_challenge_code_dotnet.Models
{
    public class UrlPlatformMetrics : AbstractBaseModel
    {
        public UrlPlatformMetrics() { }

        public UrlPlatformMetrics(Url url, string platform)
        {
            this.Url = url;
            this.UrlId = url.Id;
            this.Platform = platform;
        }

        public string Platform { get; set; }
        public DateTime Date { get; set; } = DateTime.UtcNow.Date;
        public int Count { get; set; } = 0;

        public Guid UrlId { get; set; }
        public Url Url { get; set; }
    }
}
