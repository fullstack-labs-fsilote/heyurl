﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hey_url_challenge_code_dotnet.Controllers
{
    [Route("Error")]
    public class ErrorController : Controller
    {
        [Route("404")]
        public ActionResult NotFoundPage()
        {
            return View("404");
        }

        [Route("500")]
        public ActionResult InternalServerErrorPage()
        {
            return View("500");
        }
    }
}
