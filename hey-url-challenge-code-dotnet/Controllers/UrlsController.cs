﻿using hey_url_challenge_code_dotnet.Exceptions;
using hey_url_challenge_code_dotnet.Service;
using hey_url_challenge_code_dotnet.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shyjus.BrowserDetection;
using System;
using System.Threading.Tasks;

namespace HeyUrlChallengeCodeDotnet.Controllers
{
    [Route("/")]
    public class UrlsController : Controller
    {
        private readonly IBrowserDetector browserDetector;
        private readonly IUrlService urlShortenerService;
        private readonly ILogger<UrlsController> _logger;

        public UrlsController(ILogger<UrlsController> logger, IBrowserDetector browserDetector, IUrlService urlShortenerService)
        {
            this.browserDetector = browserDetector;
            this.urlShortenerService = urlShortenerService;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var urls = await this.urlShortenerService.GetAll();

            var model = new HomeViewModel
            {
                Urls = urls
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Create(string originalUrl)
        {
            try
            {
                var shortUrl = await this.urlShortenerService.ShortenUrlAsync(originalUrl);

                TempData["Notice"] = "Your url has been shorted successfully. Please view it in the list below.";
            }
            catch (UrlShortnerException e)
            {
                TempData["Notice"] = e.Message;
            }
            catch (Exception)
            {
                TempData["Notice"] = "Sorry, we found an unexpected error. Try again later.";
            }

            return RedirectToAction("Index");
        }

        [Route("/{url}")]
        public async Task<IActionResult> Visit(string url)
        {
            var model = await this.urlShortenerService.GetByShortUrl(url);

            if (model == null)
                return NotFound();

            await this.urlShortenerService.SaveClickMetrics(url, this.browserDetector.Browser.Name, this.browserDetector.Browser.OS);

            var prefix = model.OriginalUrl.StartsWith("http") ? string.Empty : "http://";

            return Redirect($"{prefix}{model.OriginalUrl}");
        }

        [Route("urls/{url}")]
        public async Task<IActionResult> Show(string url)
        {
            var model = await this.urlShortenerService.GetByShortUrl(url);

            if (model == null)
                return NotFound();

            return View(new ShowViewModel
            {
                Url = model,
                DailyClicks = await this.urlShortenerService.CountClicksPerDayInCurrentMontByShortUrl(url),
                BrowseClicks = await this.urlShortenerService.CountClicksPerBrowserInCurrentMontByShortUrl(url),
                PlatformClicks = await this.urlShortenerService.CountClicksPerPlatformInCurrentMontByShortUrl(url)
            });
        }
    }
}